function CheckKey(e) {
  if (e.keyCode == 37) {
    previous();
  } else if (e.keyCode == 13) {
    FormProcessor();
  } else if (e.keyCode == 39) {
    next();
  }
}

function FormProcessor() {
  var x = document.getElementById("pgnum").value;
  if (isInt(x) === true) {
    if (x < 1143) {
     if (x > -33) {
        var y = x +++ 33;
       var pgnum = "page-" + y + ".pdf";
        document.getElementById("PDF").src = pgnum;
        localStorage.setItem("page", x);
        document.getElementById("Slader").src = "http://www.slader.com/textbook/9780321716569-sullivan-algebra-trigonometry-9th-edition/" + x;
      } else {
        alert("Please enter a number larger than or equal to -32");
      }
    } else {
      alert("Please enter a number smaller than or equal to 1142");
    }
  } else {
    alert("Please enter a number with no decimals");
  }
  
}

function page() {
  var answerpage = localStorage.getItem("ans");
  document.getElementById("Answers").src = "page-" + answerpage + ".pdf";
  var pagestart = localStorage.page - 2;
  var pstart33 = pagestart +++ 34;
  var pagego = "page-" + pstart33 + ".pdf";
  document.getElementById("PDF").src = pagego;
  document.getElementById("pgnum").value = pagestart;
  document.getElementById("Slader").src = "http://www.slader.com/textbook/9780321716569-sullivan-algebra-trigonometry-9th-edition/" + pagestart;
}


function isInt(value) {
  return !isNaN(value) && 
         parseInt(Number(value)) == value && 
         !isNaN(parseInt(value, 10));
}

function previous() {
  var PageNum = localStorage.page;
  var Page = PageNum - 2;
  var PageValue = Page;
  if (isInt(Page) === true) {
    if (Page < 1143) {
     if (Page > -33) {
       var Page33 = Page +++ 33;
       var PGNum = "page-" + Page33 + ".pdf";
        document.getElementById("PDF").src = PGNum;
        document.getElementById("pgnum").value = PageValue;
        localStorage.setItem("page", Page);
        document.getElementById("Slader").src = "http://www.slader.com/textbook/9780321716569-sullivan-algebra-trigonometry-9th-edition/" + PageValue;
      } else {
        alert("There are no pages before this page.");
      }
    } else {
      alert("An unknown error occured. Please try again.");
    }
  } else {
    alert("An unknown error occured. Please try again.");
  }
  
}

function next() {
  var PageNum = localStorage.page;
  var Page = PageNum;
  var PageValue = Page;
  if (isInt(Page) === true) {
    if (Page < 1143) {
     if (Page > -33) {
       var Page33 = Page +++ 33;
       var PGNum = "page-" + Page33 + ".pdf";
        document.getElementById("PDF").src = PGNum;
        document.getElementById("pgnum").value = PageValue;
        localStorage.setItem("page", Page);
        document.getElementById("Slader").src = "http://www.slader.com/textbook/9780321716569-sullivan-algebra-trigonometry-9th-edition/" + PageValue;
      } else {
        alert("An unknown error occured. Please try again.");
      }
    } else {
      alert("There are no pages after this page.");
    }
  } else {
    alert("An unknown error occured. Please try again.");
  }
  
}

function calc(e) {
  var y = Calc.input.value;
  if (e.keyCode == 13) {
    calculate();
  } else if (e.keyCode === 94) {
    Calc.input.value = "";
    var x = y + "";
    var Clac = x.replace('^',',');
    Calc.input.value = "Math.pow(" + Clac + ",";
    Remover();
  }
}


function calculate() {
  Calc.input.value = Calc.input.value + '\n' + eval(Calc.input.value) + '\n';
  document.getElementById("box").scrollTop = document.getElementById("box").scrollHeight;
}

function Remover() {
  for(i=1; i<10; i++) {
  var CalcDat = Calc.input.value;
  var CalcStr = String(CalcDat);
  var test = CalcStr.substring(0, CalcStr.length - 1);
  Calc.input.value = "";
  Calc.input.value = test + ",";
  alert("yep");
  }
}

function button() {
  var text = document.getElementById("chooser").innerHTML;
  if (text == "Slader") {
    document.getElementById("Answers").className = "Hide";
    document.getElementById("ansback").style = "opacity: 0";
    document.getElementById("ansnext").style = "opacity: 0";
    document.getElementById("Slader").className = "Show";
    document.getElementById("chooser").innerHTML = "Textbook";
  } else {
    document.getElementById("Slader").className = "Hide";
    document.getElementById("ansback").style = "opacity: 100";
    document.getElementById("ansnext").style = "opacity: 100";
    document.getElementById("Answers").className = "Show";
    document.getElementById("chooser").innerHTML = "Slader";
  }
}

function ansback() {
  var pg = localStorage.getItem("ans");
  var anspg = pg - 1;
  localStorage.setItem("ans", anspg);
  document.getElementById("Answers").src = "page-" + anspg + ".pdf";
}

function ansnext() {
  var pg = localStorage.getItem("ans");
  var anspg = pg +++ 1;
  localStorage.setItem("ans", anspg);
  document.getElementById("Answers").src = "page-" + anspg + ".pdf";
}